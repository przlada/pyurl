from furl import furl, urljoin
from url_normalize import url_normalize
from validators.url import url as validate_url

DEFAULT_SCHEME = 'http'


class PyUrl:
    def __init__(self, url_str, base_url=None):
        self._oryginal_url = url_str
        self._base_url = base_url
        self._url = ''
        if not validate_url(url_str):
            url_str = url_normalize(url_str, default_scheme=DEFAULT_SCHEME)
        if validate_url(url_str):
            self._url = url_str
        else:
            if base_url and validate_url(base_url):
                self._url = urljoin(base_url, url_str)
            else:
                raise ValueError("If link isn't absolute base url have to be absolute")
        self._url = url_normalize(self._url, default_scheme=DEFAULT_SCHEME)
        if not validate_url(self._url):
            raise ValueError("Improper url")
        self._furl = furl(self._url)

    def __str__(self):
        return self._url

    def __repr__(self):
        return f"<Url '{self._url}'>"

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self._url == other.url

    def __hash__(self):
        return hash(self._url)

    @property
    def url(self):
        return self._url

    def asdict(self):
        return self._furl.asdict()
