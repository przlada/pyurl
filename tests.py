import pytest
from pyurl import PyUrl


def test_basic():
    url_a = PyUrl('www.example.com/a')
    assert url_a.url == 'http://www.example.com/a'
    with pytest.raises(ValueError):
        PyUrl('/img/b.jpg')
    with pytest.raises(ValueError):
        PyUrl('www. @@example.om/saq')


def test_base_url():
    url_a = PyUrl('/img/b.jpg', 'https://www.example.com/b/c.html')
    assert url_a.url == 'https://www.example.com/img/b.jpg'
    with pytest.raises(ValueError):
        PyUrl('/img/b.jpg', 'www.example.com')
    url_b = PyUrl('www.example1.com/a/b/c', 'http://www.example2.com/c/b/a')
    assert url_b.url == 'http://www.example1.com/a/b/c'


def test_normalization():
    url_a = PyUrl('www.example.com/text with spaces')
    assert url_a.url == 'http://www.example.com/text%20with%20spaces'
    url_b = PyUrl('WwW.EXampLe.com/B')
    url_c = PyUrl('wWw.exAMPlE.COM/B')
    assert url_b == url_c


def test_queries():
    url_a = PyUrl('www.e.com/a?z=a&y=b&x=c')
    url_b = PyUrl('www.e.com/a?x=c&z=a&y=b')
    assert url_a == url_b


def test_comparison():
    url_b = PyUrl('WwW.EXampLe.com/B')
    url_c = PyUrl('wWw.exAMPlE.COM/B')
    assert url_c == url_b
    assert url_b == url_c
    assert hash(url_b) == hash(url_c)
    assert len(set([url_b, url_c]))


def test_fragments():
    pass


def test_structure():
    url_a = PyUrl('www.e.com/a/b/c?z=a&y=b&x=c')
    data = {
        'url': 'http://www.e.com/a/b/c?x=c&y=b&z=a',
        'scheme': 'http',
        'username': None,
        'password': None,
        'host': 'www.e.com',
        'host_encoded': 'www.e.com',
        'port': 80,
        'netloc': 'www.e.com',
        'origin': 'http://www.e.com',
        'path': {
            'encoded': '/a/b/c',
            'isdir': False,
            'isfile': True,
            'segments': ['a', 'b', 'c'],
            'isabsolute': True,
        },
        'query': {
            'encoded': 'x=c&y=b&z=a',
            'params': [('x', 'c'), ('y', 'b'), ('z', 'a')],
        },
        'fragment': {
            'encoded': '',
            'separator': True,
            'path': {
                'encoded': '',
                'isdir': True,
                'isfile': False,
                'segments': [],
                'isabsolute': []},
            'query': {
                'encoded': '',
                'params': [],
            }
        }
    }
    assert url_a.asdict() == data


