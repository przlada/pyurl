from setuptools import setup, find_packages

setup(name='PyUrl',
      version='0.2',
      description='Lib for comparing urls in python',
      url='https://www.plur.tech',
      author='Przemysław Łada',
      author_email='przlada@gmail.com',
      license='MIT',
      packages=['pyurl'],
      install_requires=['furl', 'url-normalize', 'validators',],
      zip_safe=False)
